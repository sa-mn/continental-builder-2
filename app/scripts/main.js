$(function () {

	var win = $(window),
		doc = $(document),
		htmlbody = $("html, body");


	TweenLite.defaultEase = Expo.easeOut;


	var app = {

		init: function () {

			var self = this;

			this.isTouch = Modernizr.touch;
			console.log(this.isTouch);

			this.$Header = $('.header');
			this.$Select = $('.select');
			this.$SelectedProduct = $('.selected-product');
			this.$Builder = $('.builder');


			this.selectSideNav = this.$Select.find('.side-nav');
			this.selectProducts = this.$Select.find('.products');
			this.productsRow = [];

			this.Panels = {
				yourLogoSettings: $('#your-logo-settings'),
				textInsert: $('#text-insert'),
				textSettings: $('#text-settings'),
				clipartInsert: $('#clipart-insert'),
				clipartFilter: $('#clipart-filter'),
				clipartSettings: $('#clipart-settings'),
				mobStylePanel: $('.mob-style-panel'),
				mobViewsPanel: $('.mob-views-panel')
			}


			// this.Builder_Transitions().In();
			// this.Select_Transitions().Out();


			/* Action links */
			$('.action-link').each(function () {
				$(this).on('click', function (ev) {
					ev.preventDefault();
					self.run($(this).data('action'));
				});
			});

			/* Modals: close */
			$(document).on('keydown', function (ev) {
				if(ev.keyCode == 27) {
					self.closeModal();
				}
			});


			this.selectProducts.find('.row').each(function (i, el) {

				var row = $(el),
					getProductCat = row.data('product-cat'),
					rowOffsetop = row.offset().top;

				self.productsRow.push({
					el: row,
					category: getProductCat,
					top: rowOffsetop
				});

				win.on('scroll', function () {
					if(row.is(':in-viewport(-250)')) {
						self.selectSideNav.find('ul li.active').removeClass('active');
						self.selectSideNav.find('ul li[data-product="'+getProductCat+'"]').addClass('active');
					}
				});

				row.find('.product').each(function () {
					$(this).on('click', function () {
						self.changeToSection("selected-product");
					})
				})

			});

			
			self.selectSideNav.find('ul li').each(function (i, el) {
				$(el).find('a').on('click', function (ev) {

					ev.preventDefault();
					
					self.selectSideNav.find('ul li.active').removeClass('active');
					$(el).addClass('active');

					var productRowTop = self.productsRow[i].top
					
					TweenLite.to(htmlbody, 3, { 
						scrollTop: productRowTop - 40
					});

				});
			});


			/* Select: side-nav scroll */
			win.on('scroll', function (ev) {

				var st = win.scrollTop();
				//console.log(st);
				
				if(st >= 180) {
					self.selectSideNav.addClass('fixed');
				} else {
					self.selectSideNav.removeClass('fixed');
				}

			});



			/* Select: side-nav search button */

			var sideNavSearch = this.selectSideNav.find('.search'),
				openSearchBtn = sideNavSearch.find('.open-search'),
				sideNavInput = sideNavSearch.find('.input'),
				inputCloseBtn = sideNavInput.find('.close-button');

			openSearchBtn.on('click', function () {
				$(this).hide();
				sideNavInput.show();
				sideNavInput.find('input').focus();
			});

			inputCloseBtn.on('click', function () {
				sideNavInput.hide();
				openSearchBtn.show();
			});


			/* Selected product: change colors */
			var colorSelector = this.$SelectedProduct.find('.color-selection ul');

			colorSelector.find('li').each(function () {

				$(this).find('.color').each(function () {
					$(this).css('background', $(this).data('color'));
				});

				$(this).on('click', function () {
					colorSelector.find('.selected').removeClass('selected');
					$(this).addClass('selected');
				});

			});


			/* Selected product: go to builder */
			this.$SelectedProduct.find('.controls button').on('click', function () {
				self.changeToSection("builder");
			});

		
			/* Builder: style panel */
			this.builderStylePanel = this.$Builder.find('#style-panel');

			this.builderStylePanel.find('.panel')
				.on('mouseover', function () {
					self.changeActionTitle().onHover("Click to insert a new graphic");
				})
				.on("mouseout", function () {
					self.changeActionTitle().onMouseOut();
				});

			this.builderStylePanel.find('.panel .panel__container .panel-item').each(function () {
				$(this).on('click', function () {
					
					if($(this).hasClass('active')) {
						$(this).removeClass('active');
						self.Builder_Panels().hideActivePanel()
						return;
					}

					self.builderStylePanel.find('.panel .panel-item.active').removeClass('active');
					$(this).addClass('active');

					self.Builder_Panels().changePanel($(this).data('panel'));

				});
			});

			/* Builder: colorpickers */
			$('.color-picker-handler')
				.colorpicker()
				.on('changeColor', function (e) {
					console.log(e);
					$(e.target).css('background', e.color);
				});


			/* Builder: clipart scrollbar */
			var clipartList = this.Panels.clipartInsert.find('.cliparts-list');

			

			/* Builder: dropzone */
			Dropzone.options.logoDropzone = {
				paramName: 'file',
				maxFilesize: 5,
				acceptedFiles: "image/*",
				clickable: true,
				init: function () {

					this.on('addedfile', function() {
						self.Panels.yourLogoSettings.find('.add-file-message').hide();
					});


					this.on('error', function () {
						self.Panels.yourLogoSettings.find('.dropfile-zone').hide();
						self.Panels.yourLogoSettings.find('.error-status').addClass("active");
					});

				}
			}

			/* Builder: clipart filter */
			this.Panels.clipartFilter.find('.select-filter ul li').each(function () {
				$(this).on('click', function () {
					
					var label = $(this).find('p').html();

					self.Panels.clipartFilter.find('.select-filter ul li.selected').removeClass('selected');
					$(this).addClass('selected');
					
					self.run("clipart-backto-insert");
					self.Panels.clipartInsert.find('.selected-filter p').html(label);

				})
			});



			
			/* Builder: views panel */
			this.builderViewsPanel = this.$Builder.find('#views-panel');

			this.builderViewsPanel.find('.panel')
				.on('mouseover', function () {
					self.changeActionTitle().onHover("Click to select a new view");
				})
				.on("mouseout", function () {
					self.changeActionTitle().onMouseOut();
				});

			this.builderViewsPanel.find('.panel .panel-container .view').each(function () {
				$(this)
					.on('click', function () {
						self.Builder_ChangeCapView($(this).data('view'));
						self.builderViewsPanel.find('.panel .view.selected').removeClass('selected');
						$(this).addClass('selected');
					})
					.on('mouseover', function () {
						self.builderViewsPanel.find('.panel .panel-title').html($(this).data('view'));
					})
					.on('mouseout', function () {
						self.builderViewsPanel.find('.panel .panel-title').html("Select a view");
					});
			});


			/* Builder: save button */
			this.$Builder.find('.save-button').on('click', function () {
				// Demo
				var button = $(this);
				$(this).html("Please wait...");
				setTimeout(function () {
					self.changeToSection("save");
				}, 2000);
				setTimeout(function () {
					button.html('Save <span class="icon-button-arrow"></span>');
				}, 2500);
			});


			/*  */

			this.changeActionTitle().onTransition("Select a product");

			/*  */

			var mobileMenuIcon = this.$Header.find('.mobile-menu-icon'),
				mobileCloseMenuIcon = this.$Header.find('.mobile-close-menu-icon'),
				mobileNav = $('.mobile-nav');

			mobileMenuIcon.on('click', function () {
				mobileNav.addClass('active');
				mobileMenuIcon.hide();
				mobileCloseMenuIcon.show();
				self.$Header.addClass('black');
				self.$Header.find('.action-title').hide();
			});

			mobileCloseMenuIcon.on('click', function () {
				$(this).hide();
				mobileNav.removeClass('active');
				mobileMenuIcon.show();
				self.$Header.removeClass('black');
				self.$Header.find('.action-title').show();
			});

			$('.mob-panel').each(function () {
				
				var _this = $(this);
				
				_this.find('.close-button, .see-button').on('click', function () {
					_this.removeClass('active');
				});

				_this.find('.action-link').each(function () {
					$(this).on('click', function () {
						_this.removeClass('active');
					});
				});

			});

		},



		changeToSection: function (section) {

			var self = this;

			switch(section) {

				case "select":

				this.Select_Transitions().In();
				this.SelectedProduct_Transitions().Out();
				break;

				case "selected-product":

				this.Select_Transitions().Out();
				this.SelectedProduct_Transitions().In();
				this.changeActionTitle().onTransition("Select main color");
				break;

				case "builder":

				this.Builder_Transitions().In();
				this.SelectedProduct_Transitions().Out();
				this.changeActionTitle().onTransition("Customization");
				break;

				case "save":

				this.openModal("save-design");
				this.SaveFlow().backToSavedDesign();
				break;

				case "builder-select":

				this.Builder_Transitions().Out();
				this.closeModal();
				setTimeout(function () { 
					self.Select_Transitions().In(); 
					self.changeActionTitle().onTransition("Select a product");
				}, 1000);
				break;

			}


		},


		run: function (action) {

			var self = this;

			switch (action) {

				// Change model. Back to Select product
				case "change-model":
				
				this.changeToSection("select");
				this.changeActionTitle().onTransition("Change the model")
				break;

				// Open "Acess previous design" modal
				case "modal-previous-design":

				this.openModal('access-previous-design');
				break;

				// When click on cancel button within a modal
				case "close-modal":

				this.closeModal();
				break;

				// Open final product gallery
				case "open-final-prod-gallery":
				this.SaveFlow().openFinalProductGallery();
				break;

				// Close final product gallery
				case "close-final-prod-gallery":
				this.SaveFlow().closeFinalProductGallery();
				break;

				// Cancel button on Save design flow
				case "back-to-saved":

				this.SaveFlow().backToSavedDesign();
				break;

				// Show send my email screen
				case "send-code-email":
				
				this.SaveFlow().showSendCodeByEmail();
				break;

				// Your Logo actions
				case "logo-error-try-again":
				this.Builder_Panels().logoSettings("error-try-again");
				break;

				// Text actions
				case "text-add-teamletter":
				this.Builder_Panels().textSettings("add-team-letter");
				break;

				case "text-add-customtext":
				this.Builder_Panels().textSettings("add-custom-text");
				break;

				case "text-show-fonts":
				this.Builder_Panels().textSettings("show-fonts");
				break;

				case "text-hide-fonts":
				this.Builder_Panels().textSettings("hide-fonts");
				break;

				case "text-choose-font":
				this.Builder_Panels().textSettings("choose-font");
				break;

				// Clipart

				case "clipart-show-filters":
				this.Builder_Panels().clipartSettings("show-filters");
				break;

				case "clipart-backto-insert":
				this.Builder_Panels().clipartSettings("backto-insert");
				break;

				// Mobile

				case "open-mob-views-panel":
				this.Builder_Panels().openMobilePanel("mob-views-panel");
				break;

				case "open-mob-style-panel":
				this.Builder_Panels().openMobilePanel("mob-style-panel");
				break;

				case "mob-front-view":
				self.Builder_ChangeCapView("front");
				$('.mob-btn-change-view p').html("Front view");
				break;

				case "mob-left-view":
				self.Builder_ChangeCapView("left");
				$('.mob-btn-change-view p').html("Left view");
				break;

				case "mob-right-view":
				self.Builder_ChangeCapView("right");
				$('.mob-btn-change-view p').html("Right view");
				break;

				case "mob-back-view":
				self.Builder_ChangeCapView("back");
				$('.mob-btn-change-view p').html("Back view");
				break;

				case "open-mob-my-logo":
				$('.mob-my-logo').addClass('active');
				break;

				case "open-mob-team-letter":
				$('.mob-team-letter').addClass('active');
				break;

				case "open-mob-custom-text":
				$('.mob-custom-text').addClass('active');
				break;

				case "open-mob-clipart":
				$('.mob-clipart').addClass('active');
				break;

				case "open-mob-clipart-choose-category":
				$('.mob-clipart-choose-category').addClass('active');
				break;				


				// Demo edit text
				case "demo-edit-text":
				this.Builder_Panels().textSettings("add-team-letter");
				$('.demo-text').addClass('editing');
				$('.panel-item[data-panel="text-insert"]').addClass('active');
				break;


			}

		},



		/* Select */
		Select_Transitions: function () {
			
			var self = this;

			function In() {

				self.$Header.removeClass('on-builder');
				self.$Select.show();
				htmlbody.scrollTop(0);

				if(self.isTouch) {
					self.$Select.css('opacity', 1);
					return;	
				}

				TweenMax.to(self.$Select, 1, {
					opacity: 1, delay: 0.8
				});
			}

			function Out() {

				if(self.isTouch) {
					self.$Select.css('opacity', 0).hide();
					htmlbody.scrollTop(0);
					return;
				}

				TweenMax.to(self.$Select, 1, { 
					opacity: 0,
					onComplete: function () {
						self.$Select.hide()
						htmlbody.scrollTop(0);
					}
				});
			}
			
			return {
				In: In, Out: Out
			}


		},


		/* Selected Product */
		SelectedProduct_Transitions: function () {

			var self = this;

			var productName = this.$SelectedProduct.find('.product-name'),
				productPicture = this.$SelectedProduct.find('.product-picture'),
				colorSelection = this.$SelectedProduct.find('.color-selection ul'),
				controls = this.$SelectedProduct.find('.controls');


			function In () {

				self.$Header.removeClass('on-builder');

				if(self.isTouch) {
					self.$SelectedProduct.show();
					return;
				}

				self.$SelectedProduct.show();
				TweenMax.set(productName, { x: -(win.width() / 2), opacity: 1 });
				TweenMax.set(productPicture, { opacity: 0, scale: 0.9 });
				TweenMax.set(colorSelection.find('li'), { x: colorSelection.width() + 100, opacity: 1 });
				TweenMax.set(controls.find('button, a'), { y: 350, opacity: 1 });

				var tm = new TimelineMax({ 
					delay: 1,
					onComplete: function () {
						htmlbody.css('overflow', 'hidden');
					}
				});

				tm.to(productPicture, 1, { opacity: 1, scale: 1 });
				
				colorSelection.find('li').each(function (i,el) {
					tm.to($(el), 1.4, { x: 0, delay: 1 }, 0.08 * i);
				});

				tm.to(productName, 1, { x: 0 }, 1.2);
				tm.to(controls.find('button'), 1, { y: 0 }, 1.4);
				tm.to(controls.find('a'), 1, { y: 0 }, 1.5);

			}

			function Out () {

				if(self.isTouch) {
					self.$SelectedProduct.hide();
					htmlbody.css('overflow', 'visible');
					return;
				}

				var tm = new TimelineMax({
					onComplete: function () {
						self.$SelectedProduct.hide();
						htmlbody.css('overflow', 'visible');
					}
				});

				tm.to([productName, colorSelection.find('li'), controls.find('button, a')], 1, { opacity: 0 });
				tm.to(productPicture, 1, { opacity: 0, scale: 0.9 }, 0.2);

			}

			return {
				In: In,
				Out: Out
			}

		},


		/* Builder */
		Builder_Transitions: function () {

			var self = this;

			function In() {

				self.$Header.addClass('on-builder');
				
				if(self.isTouch) {
					self.$Builder.show().css('opacity', 1);
					return;
				}
				
				self.$Builder.show();
				TweenMax.set([self.$Builder, self.$Builder.find('.cap-view')], { opacity: 0 });
				TweenMax.to(self.$Builder, 1, {
					opacity: 1, delay: 1.2,
					onComplete: function () {
						TweenMax.to(self.$Builder.find('.cap-view'), 1, {
							opacity: 1
						});
					}
				})
			}

			function Out() {

				if(self.isTouch) {
					self.$Builder.css('style', 0).hide();
					return;
				}

				TweenMax.to(self.$Builder, 0.6, {
					opacity: 0,
					onComplete: function () {
						self.$Builder.hide();
					}
				});
			}

			return {
				In: In, Out: Out
			}

		},

		Builder_ChangeCapView: function (view) {

			var capView = this.$Builder.find('.cap-view'),
				requestedView = capView.find('.view.'+view+'-view'),
				activeView = capView.find('.view.active');


			activeView.removeClass('active');
			requestedView.addClass('active');

			this.changeActionTitle().onTransition("Editing "+view+" side");

			// TweenMax.to(activeView, 0.2, { 
			// 	opacity: 0,
			// 	onComplete: function () {
			// 		activeView.removeClass('active');
			// 		requestedView.addClass('active');
			// 		TweenMax.to(requestedView, 0.2, { opacity: 1 });
			// 	}
			// });

		},

		Builder_Panels: function () {

			var self = this,
				stylePanel = this.$Builder.find('#style-panel'),
				panelSettingsItems = stylePanel.find('.panel-item-settings'),
				mobPanel = this.$Builder.find('.mob-panel');


			function _change(panel) {
				stylePanel.find('.panel-item-settings.active').hide().removeClass('active');
				stylePanel.find('#' + panel).show().addClass('active');
			}

			function _hideActive() {
				stylePanel.find('.panel-item-settings.active').hide().removeClass('active');
			}

			function _logoSettings(action) {
				switch(action) {

					case "error-try-again":
					self.Panels.yourLogoSettings.find('.error-status').removeClass('active');
					self.Panels.yourLogoSettings.find('.dropfile-zone, .add-file-message').show();
					break;

				}
			}

			function _textSettings(action) {
				switch(action) {

					case "add-team-letter":
					_change("text-settings");
					break;

					case "add-custom-text":
					_change("text-settings");
					break;

					case "show-fonts":
					self.Panels.textSettings.addClass('choose-font-active');
					self.Panels.textSettings.find('.font-style').hide();
					self.Panels.textSettings.find('.choose-font').show();
					break;

					case "hide-fonts":
					self.Panels.textSettings.removeClass('choose-font-active');
					self.Panels.textSettings.find('.font-style').show();
					self.Panels.textSettings.find('.choose-font').hide();
					break;

					case "choose-font":
					self.Panels.textSettings.removeClass('choose-font-active');
					self.Panels.textSettings.find('.font-style').show();
					self.Panels.textSettings.find('.choose-font').hide();
					break;

				}
			}

			function _clipartSettings(action) {
				switch(action) {

					case "show-filters":
					_change("clipart-filter");
					break;

					case "backto-insert":
					_change("clipart-insert");
					break;

				}
			}

			function _openMobilePanel(name) {
				
				$('.mob-panel.active').removeClass('active');
				
				switch(name) {
					
					case "mob-views-panel":
					self.Panels.mobViewsPanel.addClass('active');
					break;

					case "mob-style-panel":
					self.Panels.mobStylePanel.addClass('active');
					break;

				}

			}


			return {
				changePanel: _change,
				hideActivePanel: _hideActive,
				logoSettings: _logoSettings,
				textSettings: _textSettings,
				clipartSettings: _clipartSettings,
				openMobilePanel: _openMobilePanel
			}

		},


		/* Modal */
		openModal: function (id) {

			var self = this,
				modal = $('.modal#' + id),
				modalContainer = modal.find('.modal__container'),
				closeButton = modal.find('.top button');

			if(self.isTouch) {
				modal.css('opacity', 1).addClass('active').show();		
				return;
			}

			TweenMax.set(modal, { opacity: 0 });
			modal.show().addClass('active');

			var tm = new TimelineMax();

			tm.to(modal, 0.8, { opacity: 1 });

			closeButton.on('click', function () {
				self.closeModal();
			});

		},

		closeModal: function () {

			if(self.isTouch) {
				$('.modal.active').css('opacity', 0).hide();
				return;
			}

			TweenMax.to('.modal.active', 0.5, {
				opacity: 0,
				onComplete: function () {
					$('.modal.active').hide();
				}
			});
		},

		changeModalTitle: function (modal, title) {
			modal.find('.top .title').html(title);
		},


		/* Save Flow */
		SaveFlow: function () {

			var self = this,
				modal = $('.modal#save-design'),
				
				steps = {
					finalDesign: modal.find('.final-design'),
					desiredQnt: modal.find('.desired-quantity'),
					sendForm: modal.find('.send-form'),
					confirmation: modal.find('.confirmation'),
					sendByEmail: modal.find('.send-by-email')
				},

				controls = {
					step1: modal.find('.controls.step-1'),
					step2: modal.find('.controls.step-2'),
					step3: modal.find('.controls.step-3'),
					stepSendCode: modal.find('.controls.step-sendcode')
				},

				buttons = {
					requestQuote: controls.step1.find('.request-quote'),
					reqQuoteNext: controls.step2.find('.request-quote-next'),
					reqQuoteSend: controls.step3.find('.request-quote-send'),
					sendByEmail: controls.stepSendCode.find('.button-send-by-email'),
					createNew: steps.confirmation.find('button.create-new')
				};


			self.changeModalTitle(modal, "Request a Quote");


			/* Gallery */
			var finalPicture = steps.finalDesign.find('.final-picture'),
				gallery = steps.finalDesign.find('.final-picture-gallery'),
				viewName = gallery.find('.view-name'),
				thumbnails = gallery.find('.thumbnails');

			var swiperFullImage = new Swiper(gallery.find('.full-picture .swiper-container'), {
				spaceBetween: 0,
				speed: 900,
				nextButton: '.button-arrow-control.right',
				prevButton: '.button-arrow-control.left',
				simulateTouch: false
			});

			swiperFullImage.on('slideChangeStart', function (e) {
				
				thumbnails.find('.image.active').removeClass('active');
				thumbnails.find('.image').eq(e.activeIndex).addClass('active');

				switch(e.activeIndex) {
					case 0:
					viewName.html("Front");
					break;
					case 1:
					viewName.html("Left");
					break;
					case 2:
					viewName.html("Right");
					break;
					case 3:
					viewName.html("Back");
					break;
				}

			});

			thumbnails.find('.image').each(function (i, el) {
				$(this).on('click', function () {
					swiperFullImage.slideTo(i, 1200);
				});
			});



			buttons.requestQuote.on('click', function () {
				
				steps.finalDesign.addClass('small-design-code');
				
				steps.desiredQnt.show();
				TweenMax.to(steps.desiredQnt, 0.8, { opacity: 1, delay: 1 });

				TweenMax.set(controls.step2, { opacity: 0 });
				TweenMax.to(controls.step1, 0.8, {
					opacity: 0,
					onComplete: function () {
						controls.step1.removeClass('active');
						controls.step2.addClass('active');
						TweenMax.to(controls.step2, 0.8, { opacity: 1 });
					}
				});

				setTimeout(function () {
					swiperFullImage.update(true);
				}, 1000);

			});

			buttons.reqQuoteNext.on('click', function () {

				if(steps.desiredQnt.find('input').val().length > 1) {

					steps.finalDesign.addClass('hide-image');
					steps.sendForm.find('.quantity-display .value').html(steps.desiredQnt.find('input').val());
					
					TweenMax.set(controls.step3, { opacity: 0 });
					TweenMax.to(steps.desiredQnt, 0.8, {
						opacity: 0,
						onComplete: function () {

							steps.sendForm.show();
							TweenMax.to(steps.sendForm, 0.8, { opacity: 1 });


						}
					});
					TweenMax.to(controls.step2, 0.8, {
						opacity: 0,
						onComplete: function () {
							controls.step2.removeClass('active');
							controls.step3.addClass('active');
							TweenMax.to(controls.step3, 0.8, { opacity: 1 });
						}
					});

					return;
				}

				TweenMax.to(steps.desiredQnt.find('.input__big'), 0.2, {
					scale: 1.05, yoyo: true, repeat: 1
				});


			});

			buttons.reqQuoteSend.on('click', function () {

				TweenMax.set(steps.confirmation, { opacity: 0 });
				steps.confirmation.addClass('active').show();

				TweenMax.to(steps.sendForm, 0.8, {
					opacity: 0,
					onComplete: function () {

						modal.addClass("confirmation-step");
						modal.find('.send-form').hide();
						TweenMax.to(modal, 1, { backgroundColor: "white" });

						TweenMax.to(controls.step3, 0.8, { 
							opacity: 0,
							onComplete: function () {
								controls.step3.removeClass('active');
							}
						});
						TweenMax.to(steps.confirmation, 0.8,{
							opacity: 1, delay: 1
						});

					}
				});
				
				self.changeModalTitle(modal, "Sent");

			});

			buttons.createNew.on('click', function () {

				self.changeToSection("builder-select");
				modal.removeClass('confirmation-step');

			});


			function _backToSavedDesign() {

				steps.finalDesign.removeClass('small-design-code hide-image').show();
				TweenMax.to(steps.finalDesign, 0.6, { opacity: 1 });

				var stepsToFadeOut = [
					steps.desiredQnt,
					steps.sendByEmail,
					steps.sendForm,
					steps.confirmation
				];

				var controlsToFadeOut = [
					controls.step2,
					controls.step3,
					controls.stepSendCode
				];

				TweenMax.to(stepsToFadeOut, 1, {
					opacity: 0,
					onComplete: function () {
						for(var i in stepsToFadeOut) {
							stepsToFadeOut[i].hide();
						}
					}
				});

				TweenMax.to(controlsToFadeOut, 1, {
					opacity: 0,
					onComplete: function () {
						controls.step1.addClass('active');
						TweenMax.to(controls.step1,0.4,{ opacity: 1 });
						for(var i in controlsToFadeOut) {
							controlsToFadeOut[i].removeClass('active');
						}
					}
				});

				setTimeout(function () {
					swiperFullImage.update(true);
				}, 1000);

			}

			function _showSendCodeByEmail() {

				self.changeModalTitle(modal, "Send code by email");
				steps.sendByEmail.show();

				TweenMax.to(steps.finalDesign, 0.4, { 
					opacity: 0,
					onComplete: function () {
						TweenMax.to(steps.sendByEmail, 1, { opacity: 1 });
						steps.finalDesign.hide();
					}
				});

				TweenMax.to(controls.step1, 0.4, {
					opacity: 0,
					onComplete: function () {
						TweenMax.to(controls.stepSendCode,1,{ opacity: 1 });
						controls.step1.removeClass('active');
						controls.stepSendCode.addClass('active');
						TweenMax.to(controls.stepSendCode,1,{ opacity: 1 });
					}
				});

			}

			function _openFinalProductGallery() {

				steps.finalDesign.find('.final-picture').addClass('image-zoom');

				setTimeout(function () {
					swiperFullImage.update(true);
				}, 1000);

			}

			function _closeFinalProductGallery() {


				TweenMax.to(gallery, 0.5, {
					opacity: 0,
					onComplete: function () {
						TweenMax.to(gallery, 0.5, { opacity: 1, delay: 0.8 });
						steps.finalDesign.find('.final-picture').removeClass('image-zoom');
						setTimeout(function () {
							swiperFullImage.update(true);
						}, 500);
					}
				})


			}


			return {
				backToSavedDesign: _backToSavedDesign,
				showSendCodeByEmail: _showSendCodeByEmail,
				openFinalProductGallery: _openFinalProductGallery,
				closeFinalProductGallery: _closeFinalProductGallery
			}


		},



		/* Change action title */
		changeActionTitle: function() {

			var actionTitle = this.$Header.find('.action-title h2');

			function _onTransition(title) {

				actionTitle.data('store-message', title);

				if(self.isTouch) {
					actionTitle.html(title).css('opacity', 1);
					return;
				}
				
				TweenMax.to(actionTitle, 1, {
					opacity: 0,
					onComplete: function () {
						actionTitle.html(title);
						TweenMax.to(actionTitle, 1, { opacity: 1 });
					}
				});
			}

			function _onhover(hoverTitle) {
				actionTitle.html(hoverTitle);
			}

			function _onmouseout() {
				actionTitle.html(actionTitle.data('store-message'));
			}

			return {
				onTransition: _onTransition,
				onHover: _onhover,
				onMouseOut: _onmouseout
			}

		}



	}

	window.app = app
	app.init()



});